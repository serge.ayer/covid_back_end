const CsvReadableStream = require('csv-reader');
const AutoDetectDecoderStream = require('autodetect-decoder-stream');
const needle = require('needle');

async function readData() {
  const url =
    'https://raw.githubusercontent.com/daenuprobst/covid19-cases-switzerland/master/covid19_cases_switzerland_openzh.csv';

  const stream = needle.get(url, { compressed: false });

  let rows = [];
  stream
    .pipe(new AutoDetectDecoderStream({ defaultEncoding: '1255' })) // If failed to guess encoding, default to 1255
    .pipe(new CsvReadableStream({ parseNumbers: true, parseBooleans: true, trim: true }))
    .on('data', (row) => {
      // console.log('A row arrived: ', row);
      rows.push(row);
    })
    .on('end', function () {
      //console.log('No more rows!');
    });

  return new Promise(function(resolve, reject) {
    stream.on('done', (err) => {
      if (!err) {
        //console.log("Success");
        resolve(rows);
      } else {
        reject(err);
      }
    });
  });
}

async function getCases(startDate, endDate) {
  // get the entire data from the csv file
  let entire_data = await readData();

  // the Canton are stored in the first row of the csv file (starting at column 1)
  const NBR_OF_CANTONS = 26;
  const CANTON_ROW = 0;
  const FIRST_CANTON_COLUMN = 1;
  let cantons = entire_data[CANTON_ROW].slice(FIRST_CANTON_COLUMN, FIRST_CANTON_COLUMN + NBR_OF_CANTONS);

  // we are looking for cases per canton per day for a given period
  let data = [];
  let column = FIRST_CANTON_COLUMN + NBR_OF_CANTONS;
  let cantonIndex = 0;
  let columnName = cantons[cantonIndex] + "_diff";
  while (column < entire_data[0].length) {
    if (entire_data[0][column] === columnName) {
      // column stores the number of cases for the given canton
      let cantonData = [];
      let line = 1;
      while (line < entire_data.length) {
        // the first column of each row stores the date
        let date = new Date(entire_data[line][0]);
        date = date.toISOString().slice(0, 10);
        if ((! startDate || date >= startDate) &&
            (! endDate || date <= endDate)) {
          // console.log("cases for " + cantons[cantonIndex] + " is " + entire_data[line][column]);
          let data = {};
          data["day"] = date;
          data["cases_in_day"] = entire_data[line][column] === "" ? 0 : entire_data[line][column];
          cantonData.push(data);
        }
        line++;
      }
      data.push({"canton": cantons[cantonIndex], "cases": cantonData});
      // data[cantons[cantonIndex]] = cantonData;

      cantonIndex++;
      columnName = cantons[cantonIndex] + "_diff";
    }
    column++;
  }

  return data;
}

module.exports = {
  readData,
  getCases
};