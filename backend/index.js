const express = require('express');
const app = express();
const path = require('path');
const cors = require('cors');
const bodyParser = require('body-parser');
const OpenApiValidator = require('express-openapi-validator');
const positivesRoute = require('./routes/positives');

// enable CORS
app.use(cors());

// parse application/json body content
app.use(bodyParser.json());

// 3. (optionally) Serve the OpenAPI spec
const spec = path.join(__dirname, 'covid-data.json');
app.use('/spec', express.static(spec));

// 4. Install the OpenApiValidator onto your express app
app.use(
  OpenApiValidator.middleware({
    apiSpec: spec,
    validateRequests: true, // (default)
    validateResponses: true, // false by default
  }),
);

// 5. Define routes using routes defined with Express router
app.use('/positives', positivesRoute);

// 6. Create an Express error handler
app.use((err, req, res, next) => {
  // dump error to console for debug
  console.error(err);
  // format the error (if no status is specified, make it 500)
  res.status(err.status || 500).json({
    message: err.message,
    status: err.status,
  });
});

const port = process.env.PORT || 8080;
app.listen(port, () => {
  console.log(`Covid data app listening at http://localhost:${port}`);
});

