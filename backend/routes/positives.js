const express = require('express');
const router = express.Router();

// Require controller modules
const positivesController = require('../controllers/positivesController');

/// PLANET ROUTE

// GET planets at root
router.get('/', positivesController.positiveCases);

module.exports = router;
