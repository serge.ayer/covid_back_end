const users = require('../resources/users');
const bcrypt = require('bcrypt');

let findUserByCredentials = ({ username, password }) =>
  users.find(user => user.username === username && bcrypt.compareSync(password, user.password));
let findUserByToken = ({ userId }) =>
  users.find(user => user.id === userId);

exports.byCredentials = findUserByCredentials;
exports.byToken = findUserByToken;