const chai = require("chai");
const assertArrays = require('chai-arrays');
const moment = require("moment");

const readData = require("../models/cases").readData;
const getCases = require("../models/cases").getCases;

chai.use(assertArrays);

describe("Cases from Online CSV", () => {
  describe("readData() function", async () => {
    it("gets the correct number of days and day info from the URL", async () => {
      let entire_data = await readData();
      // check that we get the expected number of days
      const start = moment("2020-02-25");
      const end = moment("2022-07-05");
      const nbrOfDays = end.diff(start, "days");
      const expectedNbrOfRows = nbrOfDays + 1 /* count last day */ + 1 /* first row */;
      chai.expect(entire_data).to.be.ofSize(expectedNbrOfRows);
      // check that we get the expected number of rows for each day
      const expectedNbrOfColumns = 109;
      entire_data.forEach(element => {
        chai.expect(element).to.be.ofSize(expectedNbrOfColumns);
      });
    });
  });

  describe("getCases", async () => {
    it("gets the correct number of days and day info from the URL", async () => {
      // start and end must be specified as in the query parameters
      const start = "2021-07-01";
      const end = "2021-07-31";

      let cases = await getCases(start, end);
      const nbrOfCantons = 26;
      chai.expect(cases).to.be.ofSize(nbrOfCantons);

      const startMoment = moment(start);
      const endMoment = moment(end);
      const nbrOfDays = endMoment.diff(startMoment, "days") + 1;
      cases.forEach(element => {
        chai.expect(element.cases).to.be.ofSize(nbrOfDays);
      });
    });
  });
});