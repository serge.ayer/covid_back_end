const model = require('../models/cases');

// return the positive cases responses
exports.positiveCases = async (req, res) => {
  let cases = await model.getCases(req.query.start, req.query.end);
  res.send(cases);
};
